

function handleDeviceBackButton() {
    /* write you code here */
    alert('ehueheheu');
    //angular.element('[ng-controller=MainCtrl]').scope().toggleProjects();
};

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.show();
            //StatusBar.styleDefault();
        }
        
        //document.addEventListener("backbutton", handleDeviceBackButton, false);
        
    });
})

/**
 * The Projects factory handles saving and loading projects
 * from local storage, and also lets us save and load the
 * last active project index.
 */
.factory('MyMenuFactory', menuFactory)
    .factory('Projects', function () {
        return {
            all: function () {
                var projectString = window.localStorage['projects'];
                if (projectString) {
                    return angular.fromJson(projectString);
                }
                return [];
            },
            save: function (projects) {
                window.localStorage['projects'] = angular.toJson(projects);
            },
            newProject: function (projectTitle) {
                // Add a new project
                return {
                    title: projectTitle,
                    tasks: []
                };
            },
            getLastActiveIndex: function () {
                return parseInt(window.localStorage['lastActiveProject']) || 0;
            },
            setLastActiveIndex: function (index) {
                window.localStorage['lastActiveProject'] = index;
            }
        }
    })

.controller('MainCtrl', function ($scope, $timeout, $ionicModal, Projects, $ionicSideMenuDelegate, MyMenuFactory, $ionicScrollDelegate, $ionicNavBarDelegate) {

    $scope.person = {
        title: 'Op. Dr.',
        namesurname: 'Emine Özel'
    };

    $scope.personInfo = function (person) {
        return person.title + ' ' + person.namesurname;
    };

    //Side Menu
    $scope.menuItems = MyMenuFactory.menuItems();

    //Accordion
    $scope.toggleGroup = function (group) {
        $ionicScrollDelegate.scrollTop();
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
    };

    //Navigation
    $scope.page = {
        link: "partials/main.html",
        title: "Ana Sayfa"
    }
    $scope.setPage = function (address, pageTitle) {
        $scope.page.link = address;
        $scope.page.title = pageTitle;
        $ionicScrollDelegate.scrollTop();
        $ionicSideMenuDelegate.toggleLeft(false);
    }

    $scope.openLeftMenu = function () {
        $ionicSideMenuDelegate.toggleLeft(true);
    }

    $scope.toggleProjects = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };

    //Open Side Menu in 2seconds
    $scope.initApp = function () {
        $timeout(function () {
            $ionicSideMenuDelegate.toggleLeft();
        }, 1500);
    };

    /***********************************************************/

})


.controller('MailCtrl', function ($scope) {

    $scope.result = "NOT SENT";
    var i = 0;

    $scope.emailAvaible = false;
    window.plugin.email.isServiceAvailable(
        function (isAvailable) {
            $scope.emailAvaible = isAvailable;
        }
    );

    $scope.sendEmail = function () {
        var result = "TT";
        window.plugin.email.open();
        $scope.result = "SENT" + (++i) + "    " + result;
    }

})

.controller('BirthCtrl', function ($scope) {
    $scope.monthNames = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"];
    $scope.days = [];
    $scope.months = [];
    $scope.years = [];
    for (i = 0; i < 31; i++) {
        $scope.days[i] = (i + 1);
    }
    for (i = 0; i < 12; i++) {
        $scope.months[i] = i;
    }
    for (i = 0; i < 38; i++) {
        $scope.years[i] = (i + 2013);
    }
    $scope.selectedDay = 1;
    $scope.selectedMonth = 0;
    $scope.selectedYear = 2014;
    $scope.calculated = false;
    $scope.estimatedbirthdate = null;
    $scope.estimatedpregnancydate = null;
    $scope.culatebirthdate = function () {
        var selectedDate = new Date($scope.selectedYear, $scope.selectedMonth, $scope.selectedDay, 0, 0, 0, 0);
        var dayTime = 1000 * 60 * 60 * 24;
        var days13 = dayTime * 13;
        var week40 = dayTime * 7 * 40 - dayTime;
        $scope.estimatedpregnancydate = new Date(selectedDate.getTime() + days13);
        $scope.estimatedbirthdate = new Date(selectedDate.getTime() + week40);
        $scope.calculated = true;
    };
})

.directive('myModal', function () {
    return {
        templateUrl: 'new-task-template.html'
    };
})

.directive('backImg', backImgDirective)
    .directive('zoomable', scrollRender);