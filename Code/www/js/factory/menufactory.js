var menuList = [];
var menuFactory = function () {
    return {
        all: function () {
            var projectString = window.localStorage['projects'];
            if (projectString) {
                return angular.fromJson(projectString);
            }
            return [];
        },
        menuItems: function () {

            //HAKKINDA
            var personalMenu = createMenuItem('Hakkında', 'ion-person');
            createMenuItem('Özgeçmiş', 'ion-ribbon-b', personalMenu, page("cv"));
            createMenuItem('Klinik Uygulamalar', 'ion-medkit', personalMenu, page("clinic"));
            createMenuItem('Makaleler', 'ion-document-text', personalMenu, page("article"));
            //createMenuItem('İletişim', 'ion-email', personalMenu, page("contact"));
            
            //HESAPLAMALAR
            var calculationsMenu = createMenuItem("Hesaplamalar", 'ion-calculator');
            createMenuItem("Doğum Tarihi", "ion-egg", calculationsMenu, page("calculation/estimatedbirth"));
            
            
            
            //MERAK EDİLENLER
            var questionMenu = createMenuItem('Merak Edilenler', 'ion-help-circled');
            createMenuItem('Gebelik Şikayetleri', 'ion-female', questionMenu, page("pregnancy/problems"));
            createMenuItem('Tüp Bebek', 'ion-female', questionMenu, page("pregnancy/tubebaby"));
            createMenuItem('Gebelik', 'ion-female', questionMenu, page("pregnancy/pregnancy"));
            createMenuItem('PAP Smear Testi', 'ion-female', questionMenu, page("pregnancy/smear"));
            createMenuItem('Gebelikte Yapılan Testler', 'ion-female', questionMenu, page("pregnancy/tests"));
            createMenuItem('Normal/Sezeryan Doğum?', 'ion-female', questionMenu, page("pregnancy/normalcaesarean"));
            createMenuItem('Hafta Hafta Gebelik', 'ion-female', questionMenu, page("pregnancy/weekbyweek"));
            
            //GEBELİK ve DOĞUM
            var birthMenu = createMenuItem("Gebelik ve Doğum", 'ion-bonfire');
            createMenuItem('Gebelik Öncesi Danışma', 'ion-female', birthMenu, page("birth/beforepregnancy"));
            createMenuItem('Antenal Bakım', 'ion-female', birthMenu, page("birth/antenal"));
            createMenuItem('Anne Vücudundaki Değişim', 'ion-female', birthMenu, page("birth/changeinmother"));
            createMenuItem('Cilt Değişiklikleri', 'ion-female', birthMenu, page("article"));
            createMenuItem('Beslenme & Kilo Alımı', 'ion-female', birthMenu, page("birth/getweight"));
            createMenuItem('Erken Gebelik Kanamaları', 'ion-female', birthMenu, page("birth/birthbleeding"));
            createMenuItem('Düşük Tehdidi', 'ion-female', birthMenu, page("birth/abortionrisk"));
            createMenuItem('Düşükler', 'ion-female', birthMenu, page("birth/abortion"));
            createMenuItem('Dış Gebelik', 'ion-female', birthMenu, page("birth/ectopicpregnancy"));
            createMenuItem('Mol Gebelik (Üzüm Gebeliği)', 'ion-female', birthMenu, page("birth/molpregnancy"));
            createMenuItem('İleri Gebelikte Kanamalar', 'ion-female', birthMenu, page("birth/postpregnancy"));
            createMenuItem('Tarama Testleri & Risk Tespiti', 'ion-female', birthMenu, page("birth/riskanalyze"));
            createMenuItem('Gestasyonel Diabet Taraması', 'ion-female', birthMenu, page("birth/diabet"));
            
            //AİLE PLANLAMASI
            var familyPlanMenu = createMenuItem('Aile Planlaması', 'ion-leaf');
            createMenuItem('Aile Planlaması Nedir?', 'ion-female', familyPlanMenu, page("familyplan/familyplan"));
            createMenuItem('Korunma Yöntemleri', 'ion-female', familyPlanMenu, page("familyplan/protection"));
            createMenuItem('Doğum Kontrol Hapları', 'ion-female', familyPlanMenu, page("familyplan/birthcontrolpills"));
            createMenuItem('Doğum Kontrol Halkası', 'ion-female', familyPlanMenu, page("familyplan/birthcontrolcircle"));
            createMenuItem('Aylık Enjeksiyonlar', 'ion-female', familyPlanMenu, page("familyplan/monthlyinjection"));
            createMenuItem('İmplanon', 'ion-female', familyPlanMenu, page("familyplan/implanon"));
            createMenuItem('RIA (Spiral)', 'ion-female', familyPlanMenu, page("familyplan/spiral"));
            createMenuItem('Mirena', 'ion-female', familyPlanMenu, page("familyplan/mirena"));
            createMenuItem('Acil Korunma Yöntemleri', 'ion-female', familyPlanMenu, page("familyplan/urgentprotection"));
            createMenuItem('Diyafram', 'ion-female', familyPlanMenu, page("familyplan/diyafram"));
            createMenuItem('Servikal Başlık', 'ion-female', familyPlanMenu, page("familyplan/servichead"));
            createMenuItem('Kondom', 'ion-female', familyPlanMenu, page("familyplan/condom"));
            createMenuItem('Tüp Ligasyonu', 'ion-female', familyPlanMenu, page("familyplan/tubeligation"));
            createMenuItem('Vazektomi', 'ion-female', familyPlanMenu, page("familyplan/vazektomi"));
            
            //TÜP BEBEK
            var tubeBabyMenu = createMenuItem('Kısırlık & Tüp Bebek', 'ion-shuffle');
            createMenuItem('Kısırlık Nedir?', 'ion-female', tubeBabyMenu, page("tubebaby/infertility"));
            createMenuItem('Aşılama (İnseminasyon) Nedir?', 'ion-female', tubeBabyMenu, page("tubebaby/inseminasyon"));
            createMenuItem('Tüp Bebek Nedir?', 'ion-female', tubeBabyMenu, page("tubebaby/tubebaby"));            
            
            return menuList;
        }
    }

};


function createMenuItem(title, icon) {
    return createMenuItem(title, icon, null, null);
}

function page(link) {
    return "partials/" + link + ".html";
}

function createMenuItem(title, icon, parentMenuItem, pageLink) {
    var menuItem = {}
    menuItem.title = title;
    menuItem.icon = icon;
    menuItem.page = pageLink;
    menuItem.subMenuList = [];

    if (parentMenuItem != null) {
        parentMenuItem.subMenuList.push(menuItem);
    } else {        
        menuList.push(menuItem);
    }

    return menuItem;
}